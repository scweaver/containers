# CKI Project Container Images

This repo contains the build scripts for the container images used by the CKI
Project. Each container image is built and hosted in GitLab CI using
[buildah](https://github.com/containers/buildah).

A thorough description on how the CKI project uses container images can be
found in the [public documentation].

Please only add new documentation here that is specific to the CKI container
image repository!

## Project Details

### Design principles

- Only container image build files belong in the `builds` directory. Any
  additional content, such as certificates, repositories, or configuration
  files must be in the `files` directory.
- Keep container images small by cleaning up after yourself within the
  container. Watch out for applications that leave behind lots of cached data.

## Container image include files

The container image build files are split into multiple include files to create
some orthogonality and reduce duplication. This is a work in progress.

In each container image build file, exactly one of the setup include files
should be used via the first `#include` directive. This is mainly concerned
with setting up common dnf/yum configuration, certificates and environment
variables and installing a core Python 3 stack:

- `setup-from-fedora`: Common setup steps that should be included in all
  Fedora-based CKI container images.
- `setup-from-rhel`: Common setup steps that should be included in all
  RHEL-based builder images.
- `setup-from-stream`: Common setup steps that should be included in all
  CentOS Stream-based builder images.

> **NOTE:** It's important not to use any Dockerfile/Containerfile command
> (`RUN`, `COPY`, `ADD`, etc) before this first `#include` or it will break
> the build.

All setup include files include the following snippet files:

- `snippet-certs`: Add Red Hat and CKI SSL certificates to the CA trust store.
- `snippet-cki-home`: Set /cki as the home directory via `$HOME` and
  `/etc/passwd`. For containers running as root, the root `/etc/passwd` entry
  will be used. For containers running as an arbitrary user ID, at least the
  `$HOME` variable will be set correctly. To make some software work, it might
  also necessary to add the current user to `/etc/passwd` via something like

  ```shell
  if [ -w '/etc/passwd' ] && ! id -nu > /dev/null 2>&1; then
      echo "cki:x:$(id -u):$(id -g):,,,:${HOME}:/bin/bash" >> /etc/passwd
  fi
  ```

- `snippet-curlrc`: Provide a common curlrc file via `$CKI_CURL_CONFIG_FILE`
- `snippet-envvars`: Configure common environment variables.

Configuration of pipeline container images is split across the following files:

- `builder-fedora`: Common steps for the Fedora-based pipeline builder images.
  This includes the `builder-all` include file.
- `builder-7`: Common steps for all builder images based on RHEL 7.
  This includes the `builder-all` include file.
- `builder-8`: Common steps for all builder images based on RHEL 8.
  This includes the `builder-all` include file.
- `builder-9`: Common steps for all builder images based on RHEL/CentOS Stream 9.
  This includes the `builder-all` include file.
- `builder-all`: Common steps for all pipeline builder images.
  This includes the `pipeline` include file.
- `pipeline`: Common steps for all pipeline images.
- `pipeline-fedora`: Extra common setup steps that should be included in all
  **Fedora-based** images used in the pipeline.

Each container image build file should end with an `#include` directive for the
`cleanup` include file which takes care of common cleanup tasks such as
removing caches from dnf and pip.

This `snippet-shellpec` include file provides shellspec and kcov (if
`_INSTALL_KCOV` is defined) via the latest GitHub release. It is included in
the `pipeline` include file and also used in the [cki-tools] image.

The `setup-from-base` and `python-requirements` include files should only be
used for building application container images, i.e. not in the container image
repository.

## Native builders

The repository has native builders attached with tags following the pattern
`container-build-runner-{kernel-arch}`. These builders also have access to
RHEL7 and RHEL8 RPM repositories.

## Current container images

This repository currently provides the following container images:

### base

This container image is the preferred base image of derived container images
and only includes a minimal Python 3 and pip environment.

### buildah

This multi-arch container image can run buildah to build container images.

### builder-rawhide

This container image can compile upstream Linux kernels and it also includes
extra packages for Python. It is based on a Rawhide environment, as that is
the closest to the upstream kernels.

#### builder-rawhide-llvm

`builder-rawhide` with nightly clang/LLVM builds.

### builder-eln

Similar to `builder-rawhide`, but based on a [ELN] environment.

### builder-rhel[6789]

These container images can compile RHEL 6/7/8/9 Linux kernels.

#### zstream images

Some RHEL images have also zstream versions:

- RHEL 7:
  - 7.4
  - 7.6
  - 7.7
- RHEL 8:
  - 8.1
  - 8.2
  - 8.4
  - 8.6
  - 8.7
- RHEL 9:
  - 9.0
  - 9.1

### python

This multi-arch container image (only amd64/arm64) is used for all the
non-build stages of the CKI pipeline.

## Mirrored images

The container image registry connected to this repository also hosts a mirror of
various container images from Docker Hub. For details, consult the [public
documentation].

## Debugging

The container image definitions are made by `cpp preprocessor` style templates.
It allows the composition of different templates and reduce the duplications.
But, sometimes is hard to see the implications of changes to the templates, until
the changes are pushed to Gitlab and the CI pipeline tries to build the image.

To see how the final Dockerfile/Containerfile for one specific image looks like,
try the following commands:

<!-- markdownlint-disable MD014 -->
```shell
$ IMAGE="builder-rawhide"
$ export CPATH=includes
$ cpp -E -traditional -undef builds/${IMAGE}.in | sed '/^$/d' > ${IMAGE}.Dockerfile
```
<!-- markdownlint-enable MD014 -->

For this case, `builder-fedora.Dockerfile` will be the resulting Dockerfile that
would be used to build the image. It can be used with `docker`, `podman` or
`buildah` to build the image locally and test it.

This is also useful for detecting errors with the `cpp preprocessor` templates
format. It sometimes has errors and we don't see it until the image is trying
to build at Gitlab.

Another useful technique for checking and improving the templates locally is linting
the generated Dockerfile. A tool like [Hadolint] can be used for this:

<!-- markdownlint-disable MD014 -->
```shell
$ hadolint ${IMAGE}.Dockerfile
```
<!-- markdownlint-enable MD014 -->

[public documentation]: https://cki-project.org/docs/background/container-images
[ELN]: https://docs.fedoraproject.org/en-US/eln/
[Hadolint]: https://hadolint.github.io/hadolint/
[cki-tools]: https://gitlab.com/cki-project/cki-tools/-/blob/main/builds/cki-tools.in
